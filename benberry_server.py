from tkinter import *
import sys
import threading 
import time
import zmq

context = zmq.Context()

blueberryInput = ""
cranberryInput = ""

blueberryCodeStr = ""
cranberryCodeStr = ""

# blueberry connection
blueberryPort = "5546"
blueberrySocket = context.socket(zmq.REP)
blueberrySocket.bind("tcp://*:%s" % blueberryPort)

# cranberry connection
cranberryPort = "5556"
cranberrySocket = context.socket(zmq.REP)
cranberrySocket.bind("tcp://*:%s" % cranberryPort)

def openCodeWindow(pi_num):
	window = Tk()

	if pi_num == 1:
		window.title("Blueberry Pi Code")
	elif pi_num == 2:
		window.title("Cranberry Pi Code")

	e = Entry(window)
	e.pack()
	e.focus_set()

	def callback():
		global blueberryCodeStr
		global cranberryCodeStr
		if pi_num == 1:
			blueberryCodeStr = e.get()
		elif pi_num == 2:
			cranberryCodeStr = e.get()
		
		window.destroy()

	b = Button(window, text = "OK", width = 50, height = 10, command = callback)
	b.pack()
	mainloop()

def blueberryThread():
	global blueberryCodeStr;
	global cranberryCodeStr;

	while True:
		#  Wait for request from blueberry
		blueberryInput = blueberrySocket.recv().decode("utf-8") 
		print("Received request: ", blueberryInput)
		if (blueberryInput == "SINGLE_PRESS"):
			#openCodeWindow(2) # Modify cranberry's code
			blueberrySocket.send_string(blueberryInput)
		else:
			blueberrySocket.send_string(blueberryCodeStr)
			blueberryCodeStr = '';

def cranberryThread():
	global blueberryCodeStr;
	global cranberryCodeStr;

	while True:
		#  Wait for request from cranberry
		cranberryInput = cranberrySocket.recv().decode("utf-8") 
		print("Received request: ", cranberryInput)
		if (cranberryInput == "SINGLE_PRESS"):
			#openCodeWindow(1) # Modify blueberry's code
			blueberryCodeStr = 'DROP'
			cranberrySocket.send_string(cranberryInput)
		else:
			cranberrySocket.send_string(cranberryCodeStr)

t1 = threading.Thread(target=blueberryThread, name='blueberryThread') 
t2 = threading.Thread(target=cranberryThread, name='cranberryThread')   

t1.start() 
t2.start() 