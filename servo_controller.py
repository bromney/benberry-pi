import RPi.GPIO as GPIO
import time
import sys
import zmq
import urllib

status = "Not Connected"
while status == "Not Connected":
	try:
		url = "https://www.google.com"
		urllib.urlopen(url)
		status = "Connected"
	except:
		status = "Not connected"
	print status

context = zmq.Context()
blueberryPort = "5546"

print("Starting...")

socket = context.socket(zmq.REQ)
socket.connect("tcp://10.2.34.252:%s" % blueberryPort)

while True:
	socket.send_string("READY")
	message = socket.recv()
	if message == "DROP":
		socket.send_string("DROPPING!")
		message = socket.recv()

		### DROPPING START ###
		servoPIN = 17
		GPIO.setmode(GPIO.BCM)
		GPIO.setup(servoPIN, GPIO.OUT)

		p = GPIO.PWM(servoPIN, 50) # 50 Hz

		# Initialization
		p.start(4)

		time.sleep(0.8)
		p.ChangeDutyCycle(12)

		time.sleep(0.5)
		p.ChangeDutyCycle(4)

		p.stop()
		GPIO.cleanup()
		### DROPPINT END ###

		time.sleep(0.1)
	else:
		time.sleep(0.01)


