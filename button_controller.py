from gpiozero import LED
import RPi.GPIO as GPIO
import time
import sys
import zmq
import urllib

status = "Not Connected"
while status == "Not Connected":
	try:
		url = "https://www.google.com"
		urllib.urlopen(url)
		status = "Connected"
	except:
		status = "Not connected"
	print status

GPIO.setup(18, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)

led = LED(17)
led.off()

context = zmq.Context()
cranberryPort = "5556"

press_1_time = 0
pi_code = ""

print("Starting...")

socket = context.socket(zmq.REQ)
socket.connect("tcp://10.2.34.252:%s" % cranberryPort)

while True:
	if GPIO.input(18) == GPIO.HIGH:

		if press_1_time > 0 and time.time() - press_1_time > 0.2:
			socket.send_string("DOUBLE_PRESS")
			pi_code = socket.recv()
			#exec(pi_code)
			press_1_time = 0
			time.sleep(0.5)
		else:
			press_1_time = time.time()
			time.sleep(0.2)

	elif press_1_time > 0 and time.time() - press_1_time > 0.7:
		led.on()
		time.sleep(0.5)
		led.off()
		socket.send_string("SINGLE_PRESS")
		print(socket.recv())
		press_1_time = 0
